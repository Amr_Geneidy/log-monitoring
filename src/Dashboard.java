import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Dashboard {

    private final Map<String, ConcurrentLinkedQueue<Long>> sections;
    // It would be better to implement our own time-based expiry Cache.
    private final Map<Long, AtomicInteger> requestFrequency;
    private final DashboardConfiguration dashboardConfig;

    public Dashboard() {
        this(DashboardConfiguration.loadConfiguration());
    }

    public Dashboard(DashboardConfiguration dashboardConfig){
        this.dashboardConfig = dashboardConfig;
        this.sections = new ConcurrentHashMap<>();
        this.requestFrequency = new ConcurrentHashMap<>();
    }

    public void consume(AccessLogEntry logEntry) {
        String section = getSection(logEntry.getEndpoint());

        long time = logEntry.getDate().getTime();
        sections.computeIfAbsent(section, k -> new ConcurrentLinkedQueue<>()).add(time);

        long timeBucket = time / dashboardConfig.getGranularity();
        requestFrequency.computeIfAbsent(timeBucket, k -> new AtomicInteger(0)).incrementAndGet();
    }

    public DashboardDisplay cleanAndGetDisplay(){
        long now = System.currentTimeMillis();
        pruneFrequencies((now - dashboardConfig.getRequestCountWindow()) / dashboardConfig.getGranularity());
        int totalNumberOfRequests = 0;
        for(AtomicInteger i : requestFrequency.values()) {
            totalNumberOfRequests += i.get();
        }
        pruneSections(now - dashboardConfig.getSectionHitWindow());
        int max = 0;
        String mostHitSection = null;
        for(Map.Entry<String, ConcurrentLinkedQueue<Long>> entry : sections.entrySet()){
            ConcurrentLinkedQueue<Long> queue = entry.getValue();
            int size = queue.size();
            if(size > max){
                max = size;
                mostHitSection = entry.getKey();
            }
        }
        return new DashboardDisplay(mostHitSection, totalNumberOfRequests);
    }

    public DashboardConfiguration getDashboardConfig() {
        return dashboardConfig;
    }

    private void pruneSections(long last){
        sections.entrySet().removeIf(entry -> {
            entry.getValue().removeIf(time -> time < last);
            return entry.getValue().isEmpty();
        });
    }

    private void pruneFrequencies(long last){
        requestFrequency.entrySet().removeIf(entry -> entry.getKey() < last);
    }

    private String getSection(String endpoint) {
        int i = endpoint.length() - 2;
        while(i >= 0 && endpoint.charAt(i) != '/')i--;
        // TODO :: decide what is the appropriate approach to handle invalid/empty sections.
        return i > 0 ? endpoint.substring(0, i) : "";
    }
}
