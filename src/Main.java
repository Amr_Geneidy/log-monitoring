import java.text.ParseException;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Main {

    public static final String DEFAULT_LOG_FILE = "/var/log/access.log";

    public static void main(String[] args) throws ParseException {
        ConcurrentLinkedQueue<AccessLogEntry> queue = new ConcurrentLinkedQueue<>();
        Dashboard dashboard = new Dashboard();

        String logFile = System.getProperty("log_file", DEFAULT_LOG_FILE);

        Thread producer = new Thread(new LogEntryProducer(logFile, LogEntryProducer.DEFAULT_INTERVAL_TIME_MILLIS, queue));
        Thread consumer = new Thread(new LogEntryConsumer(queue, dashboard));
        Thread alerter = new Thread(new DashboardAlertingSystem(dashboard));

        producer.start();
        consumer.start();
        alerter.start();
    }

    private void usage() {
        System.out.println("watcher [FILE]");
    }
}
