public class DashboardAlertingSystem implements Runnable {
    Dashboard dashboard;
    DashboardConfiguration dashboardConfiguration;
    private volatile boolean isAlerted;
    private volatile boolean stopThread;

    public DashboardAlertingSystem(Dashboard dashboard) {
        this.dashboard = dashboard;
        isAlerted = false;
        stopThread = false;
        dashboardConfiguration = dashboard.getDashboardConfig();
    }

    @Override
    public void run() {
        try {
            while (!stopThread) {
                Thread.sleep(dashboardConfiguration.getSleepLength());
                DashboardDisplay dashboardDisplay = dashboard.cleanAndGetDisplay();
                if(dashboardDisplay.getRequestsCount() >= dashboardConfiguration.getMaxRequestsPerWindow()){
                    if(!isAlerted)
                        System.out.printf("High traffic for %s generated an alert - hits = {%d}, triggered at {%d}\n", dashboardConfiguration.getName(), dashboardDisplay.getRequestsCount(), System.currentTimeMillis());
                    isAlerted = true;
                } else {
                    if(isAlerted)
                        System.out.printf("traffic for %s is back normal at {%d}\n", dashboardConfiguration.getName(), System.currentTimeMillis());
                    isAlerted = false;
                }
                System.out.printf("Most hit section for the last %ds is %s According to %s\n", dashboardConfiguration.getSectionHitWindow()/100, dashboardDisplay.getSection(), dashboardConfiguration.getName());
            }
        } catch (Exception e) {
            e.printStackTrace();
            stopRunning();
        }
    }

    public boolean isAlerted(){
        return isAlerted;
    }

    public void setStopThread(boolean stopThread) {
        this.stopThread = stopThread;
    }

    public void stopRunning() {
        stopThread = false;
    }
}