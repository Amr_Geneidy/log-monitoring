public class DashboardConfiguration {

    // Configuration name used for logging.
    private final String name;
    // The length of window within which alert is triggered iff the traffic exceed a certain threshold.
    private final long requestCountWindow;
    // The length of window within which the most hitted section is displayed.
    private final long sectionHitWindow;
    // Deciding the accuracy for the most hitted sections.
    private final long granularity;
    // The amount of time the worker alerting thread sleeps.
    private final long sleepLength;
    // Requests threshold for a defined window.
    private final int maxRequestsPerWindow;

    private static final int TWO_MINUTES_MILLIS = 2 * 60 * 100;

    private static final String DEFAULT_NAME = "Basic";
    public static final int TEN_SECONDS_MILLIS = 1000;
    public static final int ONE_SEC_MILLIS = 100;
    public static final int MAX_REQUESTS_PER_WINDOW = 2 * 60 * 10;
    private static final DashboardConfiguration DEFAULT = new DashboardConfiguration(DEFAULT_NAME, TWO_MINUTES_MILLIS, TEN_SECONDS_MILLIS, ONE_SEC_MILLIS, TEN_SECONDS_MILLIS, MAX_REQUESTS_PER_WINDOW);

    public DashboardConfiguration(String name,
                                  long requestCountWindow,
                                  long sectionHitWindow,
                                  long granularity,
                                  long sleepLength,
                                  int maxRequestsPerWindow){
        this.name = name;
        this.requestCountWindow = requestCountWindow;
        this.sectionHitWindow = sectionHitWindow;
        this.granularity = granularity;
        this.sleepLength = sleepLength;
        this.maxRequestsPerWindow = maxRequestsPerWindow;
    }

    public String getName() {
        return name;
    }

    public long getRequestCountWindow() {
        return requestCountWindow;
    }

    public long getSectionHitWindow() {
        return sectionHitWindow;
    }

    public long getGranularity() {
        return granularity;
    }

    public long getSleepLength() {
        return sleepLength;
    }

    public int getMaxRequestsPerWindow() {
        return maxRequestsPerWindow;
    }

    public DashboardConfiguration clone(){
        return new DashboardConfiguration(name, requestCountWindow, sectionHitWindow, granularity, sleepLength, maxRequestsPerWindow);
    }

    public static DashboardConfiguration loadDefaultConfiguration(){
        return DEFAULT;
    }

    public static DashboardConfiguration loadConfiguration(){
        return new DashboardConfiguration(System.getProperty("config_name", DEFAULT_NAME),
                Long.parseLong(System.getProperty("requestCountWindow", String.valueOf(TWO_MINUTES_MILLIS))),
                Long.parseLong(System.getProperty("sectionHitWindow", String.valueOf(TEN_SECONDS_MILLIS))),
                Long.parseLong(System.getProperty("granularity", String.valueOf(ONE_SEC_MILLIS))),
                Long.parseLong(System.getProperty("sleepLength", String.valueOf(TEN_SECONDS_MILLIS))),
                Integer.parseInt(System.getProperty("maxRequestsPerWindow", String.valueOf(MAX_REQUESTS_PER_WINDOW))));
    }
}
