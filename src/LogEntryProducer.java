import java.io.File;
import java.io.RandomAccessFile;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Worker class to tail a file, publishing the tailed logs to a concurrent shared Queue.
 *
 */
public class LogEntryProducer implements Runnable {

    public static final int DEFAULT_INTERVAL_TIME_MILLIS = 100;
    /**
     * Full Path of the file to watch
     */
    private final String fileName;

    /**
     * Monitors the update of the file for every intervalTime milliseconds.
     */
    private final int intervalTime;
    private final ConcurrentLinkedQueue<AccessLogEntry> queue;

    /**
     * Set the flag to true, to stop the execution of thread.
     */
    private volatile boolean stopThread = false;

    /**
     * File To watch
     */
    private File fileToWatch;

    /**
     * Variable used to get the last know position of the file
     */
    private long lastKnownPosition = 0;

    public LogEntryProducer(String fileName, int intervalTime, ConcurrentLinkedQueue<AccessLogEntry> queue) {
        this.fileName = fileName;
        this.intervalTime = intervalTime;
        this.fileToWatch = new File(fileName);
        this.queue = queue;
    }

    @Override
    public void run() {

        if (!fileToWatch.exists()) {
            throw new IllegalArgumentException(fileName + " not exists");
        }
        try {
            while (!stopThread) {
                Thread.sleep(intervalTime);
                long fileLength = fileToWatch.length();

                /**
                 * This case occur, when file is taken backup and new file
                 * created with the same name.
                 */
                if (fileLength < lastKnownPosition) {
                    lastKnownPosition = 0;
                }
                if (fileLength > lastKnownPosition) {
                    RandomAccessFile randomAccessFile = new RandomAccessFile(fileToWatch, "r");
                    randomAccessFile.seek(lastKnownPosition);
                    String line;
                    while ((line = randomAccessFile.readLine()) != null) {
                        queue.add(AccessLogEntry.parse(line));
                    }
                    lastKnownPosition = randomAccessFile.getFilePointer();
                    randomAccessFile.close();
                }
            }
        } catch (Exception e) {
            // TODO:: use a logger
            e.printStackTrace();
            stopRunning();
        }

    }

    public void setStopThread(boolean stopThread) {
        this.stopThread = stopThread;
    }

    public void stopRunning() {
        stopThread = false;
    }

}