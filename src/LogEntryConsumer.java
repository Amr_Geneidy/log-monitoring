import java.util.concurrent.ConcurrentLinkedQueue;

public class LogEntryConsumer implements Runnable {

    private int waitTime = 100;
    private ConcurrentLinkedQueue<AccessLogEntry> logQueue;
    private Dashboard dashboard;
    private volatile boolean stopThread = false;

    public LogEntryConsumer(ConcurrentLinkedQueue<AccessLogEntry> queue, Dashboard dashboard) {
        this.logQueue = queue;
        this.dashboard = dashboard;
    }

    @Override
    public void run() {
        AccessLogEntry curEntry;
        try {
            while (!stopThread) {
                Thread.sleep(waitTime);
                while ((curEntry = logQueue.poll()) != null) {
                    dashboard.consume(curEntry);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            stopRunning();
        }
    }

    public void setStopThread(boolean stopThread) {
        this.stopThread = stopThread;
    }

    public void stopRunning() {
        stopThread = false;
    }
}
