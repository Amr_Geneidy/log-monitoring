public class DashboardDisplay {

    private final String section;
    private final int requestsCount;

    public DashboardDisplay(String section, int requestsCount) {
        this.section = section;
        this.requestsCount = requestsCount;
    }

    public int getRequestsCount() {
        return requestsCount;
    }

    public String getSection() {
        return section;
    }
}
