import java.lang.String;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AccessLogEntry {

    // 09/May/2018:16:00:39 +0000
    private static final String DATE_PATTERN = "dd/MMMMM/yyyy:HH:mm:ss Z";
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(DATE_PATTERN);

    /** Example of w3c-formatted HTTP access log
     * 127.0.0.1 - james [09/May/2018:16:00:39 +0000] "GET /report HTTP/1.0" 200 1234
     * 1:IP  2:client 3:user 4:date                         5:method 6:req 7:proto   8:respcode 9:size
     */
    private static final String LOG_ENTRY_PATTERN =
            "^(\\S+) (\\S+) (\\S+) \\[([\\w:/]+\\s[+\\-]\\d{4})\\] \"(\\S+) (\\S+) (\\S+)\" (\\d{3}) (\\d+)";
    private static final Pattern ENTRY_PATTERN = Pattern.compile(LOG_ENTRY_PATTERN);

    private String ipAddress;
    private String clientIdentd;
    private String userID;
    private Date date;
    private String method;
    private String endpoint;
    private String protocol;
    private int responseCode;
    private long contentSize;

    private AccessLogEntry(String ipAddress, String clientIdentd, String userID,
                           Date date, String method, String endpoint,
                           String protocol, int responseCode,
                           long contentSize) {
        this.ipAddress = ipAddress;
        this.clientIdentd = clientIdentd;
        this.userID = userID;
        this.date = date;
        this.method = method;
        this.endpoint = endpoint;
        this.protocol = protocol;
        this.responseCode = responseCode;
        this.contentSize = contentSize;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public String getClientIdentd() {
        return clientIdentd;
    }

    public String getUserID() {
        return userID;
    }

    public Date getDate() {
        return date;
    }

    public String getMethod() {
        return method;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public String getProtocol() {
        return protocol;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public long getContentSize() {
        return contentSize;
    }

    public static AccessLogEntry parse(String logLine) throws ParseException {
        Matcher m = ENTRY_PATTERN.matcher(logLine);
        if (!m.find()) {
            throw new AccessLogParserExpection("Error; The log line didn't match the excepted format");
        }
        return new AccessLogEntry.Builder()
                .setIpAddress(m.group(1))
                .setClientIdentd(m.group(2))
                .setUserID(m.group(3))
                .setDate(DATE_FORMAT.parse(m.group(4)))
                .setMethod(m.group(5))
                .setEndpoint(m.group(6))
                .setProtocol(m.group(7))
                .setResponseCode(Integer.parseInt(m.group(8)))
                .setContentSize(Long.parseLong(m.group(9)))
                .build();
    }

    @Override public String toString() {
        return String.format("%s %s %s [%s] \"%s %s %s\" %s %s",
                ipAddress, clientIdentd, userID, date, method, endpoint,
                protocol, responseCode, contentSize);
    }

    public static class Builder {
        private String ipAddress;
        private String clientIdentd;
        private String userID;
        private Date date;
        private String method;
        private String endpoint;
        private String protocol;
        private int responseCode;
        private long contentSize;

        public Builder setIpAddress(String ipAddress) {
            this.ipAddress = ipAddress;
            return this;
        }

        public Builder setClientIdentd(String clientIdentd) {
            this.clientIdentd = clientIdentd;
            return this;
        }

        public Builder setUserID(String userID) {
            this.userID = userID;
            return this;
        }

        public Builder setDate(Date date) {
            this.date = date;
            return this;
        }

        public Builder setMethod(String method) {
            this.method = method;
            return this;
        }

        public Builder setEndpoint(String endpoint) {
            this.endpoint = endpoint;
            return this;
        }

        public Builder setProtocol(String protocol) {
            this.protocol = protocol;
            return this;
        }

        public Builder setResponseCode(int responseCode) {
            this.responseCode = responseCode;
            return this;
        }

        public Builder setContentSize(long contentSize) {
            this.contentSize = contentSize;
            return this;
        }
        AccessLogEntry build() {
            return new AccessLogEntry(ipAddress, clientIdentd, userID,
                     date,  method,  endpoint,
                     protocol,  responseCode,
                     contentSize);
        }
    }
}